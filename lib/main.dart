import 'package:flutter/material.dart';
import 'package:oech_app/view/colors.dart';
import 'package:oech_app/view/screens/onboard_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: AppColors.primaryColor,
        scaffoldBackgroundColor: Colors.white,
        hintColor: AppColors.lightTextColor,
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        scaffoldBackgroundColor: const Color(0xFF000D1D),
        hintColor: AppColors.darkTextColor,
      ),
      themeMode: ThemeMode.light,
      home: Builder(
        builder: (context) {
          return OnboardScreen();
        },
      ),
    );
  }
}
