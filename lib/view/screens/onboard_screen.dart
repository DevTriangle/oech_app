import 'package:flutter/material.dart';
import 'package:oech_app/view/widgets/onboard_widget.dart';

class OnboardScreen extends StatefulWidget {
  const OnboardScreen({super.key});

  @override
  State<OnboardScreen> createState() => _OnboardScreenState();
}

class _OnboardScreenState extends State<OnboardScreen> {
  final List<Map<String, dynamic>> screens = [
    {"title": "Quick Delivery At Your Doorstep", "desc": "Enjoy quick pick-up and delivery to your destination", "image": "1.svg"},
    {"title": "Flexible Payment", "desc": "Different modes of payment either before and after delivery without stress", "image": "2.svg"},
    {"title": "Real-time Tracking", "desc": "Track your packages/items from the comfort of your home till final destination", "image": "3.svg"},
  ];

  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: PageView.builder(
                  itemCount: 3,
                  onPageChanged: (i) {
                    currentIndex = i;
                    setState(() {});
                  },
                  itemBuilder: (b, index) {
                    return OnboardWidget(
                      title: screens[index]["title"],
                      desc: screens[index]["desc"],
                      index: index,
                      count: 3,
                      image: screens[index]["image"],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
