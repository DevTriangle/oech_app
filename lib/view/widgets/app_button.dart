import 'package:flutter/material.dart';
import 'package:oech_app/view/widgets/shapes.dart';

import '../colors.dart';

class AppButton extends StatelessWidget {
  final Color color;
  final Color bColor;
  final String label;
  final TextStyle tStyle;
  final EdgeInsets padding;
  final Function() onPressed;

  const AppButton({
    super.key,
    this.color = AppColors.primaryColor,
    required this.label,
    required this.onPressed,
    this.tStyle = const TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w600),
    this.padding = const EdgeInsets.all(16),
    this.bColor = Colors.transparent,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        decoration: BoxDecoration(
          color: color,
          border: Border.all(
            width: 1,
            color: bColor,
          ),
          borderRadius: const BorderRadius.all(AppShapes.smallBorderRadius),
        ),
        child: Padding(
          padding: padding,
          child: Center(
            child: Text(
              label,
              style: tStyle,
            ),
          ),
        ),
      ),
    );
  }
}
