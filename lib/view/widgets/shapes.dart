import 'package:flutter/material.dart';

class AppShapes {
  static const Radius smallBorderRadius = Radius.circular(4);
  static const RoundedRectangleBorder smallShape = RoundedRectangleBorder(borderRadius: BorderRadius.all(smallBorderRadius));
}
