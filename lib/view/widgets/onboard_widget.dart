import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_app/view/colors.dart';
import 'package:oech_app/view/widgets/app_button.dart';

class OnboardWidget extends StatelessWidget {
  final String title;
  final String desc;
  final int index;
  final int count;
  final String image;

  const OnboardWidget({super.key, required this.title, required this.desc, required this.index, required this.count, required this.image});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: 88),
        SizedBox(
          width: 340,
          height: 300,
          child: SvgPicture.asset(
            "assets/images/${image}",
            width: 340,
            height: 300,
            fit: BoxFit.cover,
          ),
        ),
        SizedBox(height: 48),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 22),
          height: 48,
          child: Text(
            title,
            style: TextStyle(
              fontSize: 24,
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.w600,
              height: 1,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: 5),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 22),
          height: 50,
          child: Text(
            desc,
            style: TextStyle(
              fontSize: 16,
              color: Theme.of(context).hintColor,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: 49),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(
            count,
            (i) => Card(
              elevation: 0,
              color: index == i ? Theme.of(context).primaryColor : const Color(0xFFA7A7A7),
              clipBehavior: Clip.antiAlias,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
              child: Container(width: 8, height: 8),
            ),
          ),
        ),
        SizedBox(height: index == 3 ? 82 : 62),
        index != count - 1
            ? Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  AppButton(
                    label: "Skip",
                    tStyle: TextStyle(color: AppColors.primaryColor, fontSize: 9, fontWeight: FontWeight.w600),
                    color: Colors.white,
                    bColor: AppColors.primaryColor,
                    onPressed: () {},
                    padding: EdgeInsets.symmetric(vertical: 9, horizontal: 19),
                  ),
                  AppButton(
                    label: "Next",
                    tStyle: TextStyle(color: Colors.white, fontSize: 9, fontWeight: FontWeight.w600),
                    onPressed: () {},
                    padding: EdgeInsets.symmetric(vertical: 9, horizontal: 19),
                  ),
                ],
              )
            : Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: AppButton(
                          label: "Sign Up",
                          onPressed: () {
                            print("object");
                          },
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 8),
                  Wrap(
                    children: [
                      Text(
                        "Already have an account? ",
                        style: TextStyle(
                          color: Color(0xFFA7A7A7),
                        ),
                      ),
                      InkWell(
                        onTap: () {},
                        child: Text(
                          "Sign in",
                          style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
        SizedBox(height: index == 3 ? 69 : 47),
      ],
    );
  }
}
