import 'dart:ui';

class AppColors {
  static const primaryColor = Color(0xFF0560FA);

  static const lightTextColor = Color(0xFF3A3A3A);
  static const darkTextColor = Color(0xFF3A3A3A);
}
